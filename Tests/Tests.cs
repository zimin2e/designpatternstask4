using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

namespace Tests
{


    public class MyTestClass : TestBase
    {

        public MyTestClass(ITestOutputHelper testOutputHelper) : base(testOutputHelper){}

     
     
        [Fact]
        public void TestMacroCommand()
        {
            DataRepository repository = new DataRepository();
            
            repository.setValue( "Fuel", 5); 
                       
            object[] commandParameters = new object[] { repository.getValue("Fuel") };

            var GameMacroCommand = new GameMacroCommand(repository, commandParameters);
        
            GameMacroCommand.Execute(
                new List<string>
                {
                    CommandNames.MoveCommand,
                    CommandNames.RotateCommand,
                    "MyCommandThatIsNotImplemented"
                }
            );
            //Assert.True(true);
        }

       

    }
}
