﻿using System;

namespace DataLayer
{  
    public class DataItem
    {
        public string InstanceName;
        public object Value;   
        public string TypeName;
        public Type type;
    }

}
