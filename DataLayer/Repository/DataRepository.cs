﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DataLayer
{
    public class DataRepository : IReposotory
    {

        // instance name, DataItem
        public  Dictionary<string, DataItem> items = new Dictionary<string, DataItem>();

        public void setValue(string itemName, object value)
        {
            try 
            {
                DataItem item = null;
                if( items.ContainsKey( itemName ) )
                {
                    item = items[itemName];
                    item.Value = value;
                }
                else
                {
                    item = new DataItem();
                    item.Value = value;
                    items.Add(itemName, item);
                }
               

            }catch{}               
        }


        public  DataItem getValue(string itemName)
        {
            DataItem item = null;

            if( items.ContainsKey( itemName ) )
                item = items[itemName];

            return item;
        }

/*
        public static Type GetTypeByName(string typeName)
        {
            try{
                
                System.Reflection.Assembly assembly = Assembly.GetExecutingAssembly();
                
                var tt = assembly.GetType(typeName);
                
                if (tt != null)
                {
                    return tt;
                }            

            }catch(Exception e)
            {
                throw new Exception("Can not find Assembly");
            }
            return null;
        }
*/

    }

}
