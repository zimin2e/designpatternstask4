﻿using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
    public interface IReposotory 
    {
        DataItem getValue(string itemName);
        void setValue(string itemName, object value);
    }

}
