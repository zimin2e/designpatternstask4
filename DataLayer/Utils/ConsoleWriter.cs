using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

using DataLayer;

namespace Tests
{
    public class ConsoleWriter : StringWriter
    {
        private ITestOutputHelper output;
        public ConsoleWriter(ITestOutputHelper output)
        {
            this.output = output;
        }

        public override void WriteLine(string m)
        {
            output.WriteLine(m);
        }
    }


}
