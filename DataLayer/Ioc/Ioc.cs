﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Ioc_Containers
{
    public static partial class Ioc
    {	
		
        private static readonly Dictionary<Type, Type> typesDict = new Dictionary<Type, Type>();


        /// <summary>
        /// Bind Interface to Class
        /// </summary>
        /// <typeparam name="TSource">Interface</typeparam>
        /// <typeparam name="TDestination">Class</typeparam>
        public static void Bind<TSource, TDestination>()
        {
            typesDict[typeof(TSource)] = typeof(TDestination);
        }



        public static Type GetTypeByName(string typeName)
        {

            try{
                
                System.Reflection.Assembly assembly = Assembly.GetExecutingAssembly();
                
                var tt = assembly.GetType(typeName);
                
                if (tt != null)
                {
                    return tt;
                }            

            }catch(Exception e)
            {
                throw new Exception("Can not find Assembly");
            }
            return null;
        }


        public static object Resolve(string typeName, params object[] constructorParameters)
        {
            object obj = null;
            try{
                Type type = GetTypeByName( typeName);

                // get public constructors
                var constructors = type.GetConstructors(BindingFlags.Public);

                // invoke the first public constructor with no parameters.
                obj = constructors[0].Invoke(constructorParameters);
            }catch(Exception e)
            {
                throw new Exception(typeName);
            }
            
            return obj;

        }



        /// <summary>
        /// Create instance that inherits interface
        /// </summary>
        /// <typeparam name="TSource">Interface</typeparam>
        /// <returns></returns>
        public static TSource GetInstance<TSource>()
        {
            return (TSource)GetSingleObject(typeof(TSource));
        }


        private static object GetSingleObject(Type targetType)
        {
            var destination = typesDict[targetType];

            return Activator.CreateInstance(destination);
        }




        /// <summary>
        /// Resolve dependencies with params in ctors
        /// </summary>
        /// <typeparam name="T">Interface to resolve</typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }


        private static object Resolve(Type targetType)
        {
            var destination = typesDict[targetType];

            var ctor = destination.GetConstructors().First();

            var ctorParams = ctor.GetParameters();

            if (ctorParams.Length == 1)
                return Activator.CreateInstance(destination);

            var parameters = new List<object>(ctorParams.Length);

            foreach (var param in ctorParams)
            {
                parameters.Add(Resolve(param.ParameterType));
            }

            return ctor.Invoke(parameters.ToArray());
        }



				
    }
}
