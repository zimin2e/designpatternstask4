
using System;

namespace DataLayer {

    public class IocResolveException : System.Exception
    {

        string resolvedType;

        public IocResolveException(string resolvedType) : base(resolvedType) 
        {
            Console.WriteLine("Can not resolve type: " + resolvedType);
        }       

        public IocResolveException(string resolvedType, System.Exception inner) : base(resolvedType, inner) { }

        protected IocResolveException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
          

    
}