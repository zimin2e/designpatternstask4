
using System;

namespace DataLayer {

    public class CommandNotFoudException : System.Exception
    {

        string commandName;

        public CommandNotFoudException(string commandName) : base(commandName) 
        {
            Console.WriteLine("Command not found: " + commandName);
        }       

        public CommandNotFoudException(string commandName, System.Exception inner) : base(commandName, inner) { }

        protected CommandNotFoudException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
          

    
}