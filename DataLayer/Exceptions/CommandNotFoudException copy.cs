
using System;

namespace DataLayer {

    public class FuelTankEmptyException : System.Exception
    {

        string commandName;

        public FuelTankEmptyException(string commandName) : base(commandName) 
        {
            Console.WriteLine("Fuel tank is empty: " + commandName);
        }       

        public FuelTankEmptyException(string commandName, System.Exception inner) : base(commandName, inner) { }

        protected FuelTankEmptyException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
          

    
}