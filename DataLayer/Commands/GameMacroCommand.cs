using System.Collections.Generic;
using System;
using Ioc_Containers;

namespace DataLayer {
    public class GameMacroCommand : IMacroCommand 
    {

        public DataRepository repository;

        Dictionary<string, ICommand> _commands = new Dictionary<string, ICommand> ();

        object[] commandParameters;
        public GameMacroCommand(DataRepository repository, params object[] commandParameters)
        {
            this.repository = repository;
            this.commandParameters = commandParameters;
        }

        public ICommand GetCommand(string commandName)
        {

            if(!_commands.ContainsKey(commandName) )
            {
                _commands.Add(commandName, (ICommand)Ioc.Resolve(commandName, commandParameters));
            }
               // throw new CommandNotFoudException(commandName);

            return _commands[commandName];
        }

        public void CreateCommands(List<string> commandNames)
        {
            foreach(var commandName in commandNames)
            {
                _commands.Add(commandName, GetCommand(commandName) );
            }

        }

        public List<ICommand> FindCommands(List<string> commandNames)
        {
            var commands = new List<ICommand>();

            foreach(var commandName in commandNames)
            {
                try
                {
                    commands.Add (GetCommand(commandName) );

                }catch(CommandNotFoudException e)
                {
                    Console.WriteLine(commandName);
                }
            }

            return commands;
        }


        public void ExecuteCommands(List<string> commandNames)
        {
            foreach(var commandName in commandNames)
            {
                _commands.Add(commandName, GetCommand(commandName) );
            }

        }


        List<string> neededCommands = new List<string> ();

        public void Execute(List<string> commandnames)
        {
            FindCommands(commandnames);

            foreach(ICommand command in _commands.Values)
            {
                command.Execute();
            }
            
        }
        
    }
}