using System;

namespace DataLayer {

    public interface ICommand {
        
        void Execute();

    }


}