using System.Collections.Generic;

namespace DataLayer {

    public interface IMacroCommand 
    {        
        void Execute(List<string> commandNames);
    }


}