using System;

namespace DataLayer 
{

    public class RotateCommand : ICommand
    {
        DataRepository repository {get;set;}
        int FuelToBurn;
        public RotateCommand (DataRepository repository, int FuelToBurn)
        {
            this.repository = repository;
            this.FuelToBurn = FuelToBurn;
        }

        public void Execute()
        {           
            var Fuel = (int) repository.getValue( "Fuel" ).Value;

            Fuel = Fuel - FuelToBurn;

            Console.WriteLine("Fuel QTY after rotete:" + Fuel + "liters");
        }

    }


}